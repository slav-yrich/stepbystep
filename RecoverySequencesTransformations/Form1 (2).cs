﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace StepByStep
{
    public partial class Form1 : Form
    {
        List<List<double>> intensMatrix;
        string[] models, fileList;
        double[,] kMatrix;

        public Form1()
        {
            InitializeComponent();

            intensMatrix = null;
            models = null;
            fileList = null;
            kMatrix = null;
        }
        private double Norma(List<double> x)
        {
            double sum = 0;
            int i;
            for (i = 0; i <= x.Count - 1; i++)
            {
                sum += x[i] * x[i];
            }
            return Math.Sqrt(sum);
        }
        private double Norma(List<double> x1, List<double> x2)
        {
            double sum = 0;
            int i;
            for (i = 0; i <= x1.Count - 1; i++)
            {
                sum += (x1[i] - x2[i]) * (x1[i] - x2[i]);
            }
            return Math.Sqrt(sum);
        }
        private double DerNorma(List<double> x1, List<double> x2)
        {
            return Norma(x1, x2) / Math.Sqrt(Norma(x1) * Norma(x2));
        }

        private void загрузитьФайлыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            lbOriginalList.Items.Clear();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //try
                {
                    //MessageBox.Show(Path.GetFileName(ofd.FileNames[0]) + " | " + ofd.FileNames[ofd.FileNames.Length - 1]);
                    intensMatrix = new List<List<double>>();
                    fileList = ofd.FileNames;
                    StreamReader intensFile;

                    DataTable dt = new DataTable();
                    DataRow r;
                    dt.Columns.Add("q");

                    //формируем список моделей по списку файлов
                    models = new string[ofd.FileNames.Length];
                    for (int i = 0; i < ofd.FileNames.Length; i++)
                    {
                        models[i] = Path.GetFileName(ofd.FileNames[i]);
                        models[i] = "model_" + models[i].Substring(models[i].IndexOf("model") + "model".Length + 1, models[i].Substring(models[i].IndexOf("model") + "model".Length + 1).IndexOf('_'));

                        dt.Columns.Add(models[i]);
                        lbOriginalList.Items.Add(models[i]);//Path.GetFileName(ofd.FileNames[i]));
                    }

                    string[] formatline;
                    string line;

                    //считываем интенсивности в матрицу + создаем пустую заготовку для вывода таблицы 
                    for (int i = 0; i < ofd.FileNames.Length; i++)
                    {

                        intensFile = new StreamReader(ofd.FileNames[i]);
                        intensMatrix.Add(new List<double>());
                        while ((line = intensFile.ReadLine()) != null)
                        {
                            if (line != "")
                            {
                                formatline = line.Trim().Split('\t');
                                intensMatrix[i].Add(Convert.ToDouble(formatline[1]));

                                if (i == 0)
                                {
                                    r = dt.NewRow();
                                    r["q"] = formatline[0];
                                    dt.Rows.Add(r);
                                }
                            }
                        }

                        intensFile.Close();
                    }

                    //просто выводим таблицу)
                    for (int i = 0; i < intensMatrix.Count; i++)
                    {
                        for (int j = 0; j < intensMatrix[i].Count; j++)
                        {
                            dt.Rows[j][models[i]] = intensMatrix[i][j].ToString();
                        }
                    }
                    dataGridView1.DataSource = dt;
                    dt.Dispose();

                    kMatrix = new double[models.Length, models.Length];
                    bool calcOrRead = true;
                    if (!calcOrRead)//Загружаем матрицу коэффициентов отличия  из файла
                    {

                    }
                    else
                    {
                        //считаем коэффициент отличия "каждый с каждым"

                        for (int i = 0; i < models.Length - 1; i++)
                        {
                            kMatrix[i, i] = 0;
                            for (int j = i + 1; j < models.Length; j++)
                            {
                                //for (int iq = 0; iq < intensMatrix[i].Count; iq++)
                                //{
                                //    kMatrix[i, j] += Math.Abs(intensMatrix[i][iq] - intensMatrix[j][iq]);
                                //}
                                //kMatrix[i, j] /= intensMatrix[i].Count;
                                //kMatrix[j, i] = kMatrix[i, j];
                                kMatrix[i, j] = DerNorma(intensMatrix[i], intensMatrix[j]);
                                kMatrix[j, i] = kMatrix[i, j];
                            }
                        }
                    }
                    //intensMatrix.Clear(); //очищаем матрицу интенсивностей за ненадобностью

                    dt = new DataTable();
                    dt.Columns.Add("model");
                    for (int i = 0; i < models.Length; dt.Columns.Add((i++).ToString())) ; //добавляем столбцы
                    //выводим матрицу коэффициентов отличия 
                    for (int j = 0; j < models.Length; j++)
                    {
                        r = dt.NewRow();
                        r["model"] = j.ToString();
                        for (int i = 0; i < models.Length; i++)
                        {
                            r[i.ToString()] = kMatrix[i, j];
                        }
                        dt.Rows.Add(r);
                    }
                    dataGridView2.DataSource = dt;
                    dt.Dispose();

                    //Начинаем искать оптимальный переход по заданной матрице (графу) коэффициентов отличия
                    List<int> minWay = new List<int>(),
                             currentWay = new List<int>();
                    int currentStep;
                    double minSum = double.MaxValue,
                           currentSum = 0;
                    double minStep;
                    int numbMinStep;
                    for (int dot = 0; dot < models.Length; dot++)
                    {
                        currentWay.Clear();
                        currentWay.Add(dot);
                        currentSum = 0;

                        currentStep = dot;
                        for (int i = 0; i < models.Length/* - 1*/; i++)
                        {
                            minStep = double.MaxValue;
                            numbMinStep = -1;

                            for (int j = 0; j < models.Length; j++)
                            {
                                if ((currentStep != j) && (kMatrix[currentStep, j] < minStep))
                                {

                                    bool flag = false;
                                    for (int g = 0; ((g < currentWay.Count)); g++)
                                    {
                                        if (!(flag = (j != currentWay[g])))
                                        {
                                            break;
                                        }
                                        //MessageBox.Show(j.ToString() + "| [" + g.ToString() + "]" + currentWay[g].ToString());
                                    }

                                    if (flag)
                                    {
                                        minStep = kMatrix[currentStep, j];
                                        numbMinStep = j;
                                    }
                                }
                            }
                            //this.Refresh();
                            if (numbMinStep != -1)
                            {
                                currentWay.Add(numbMinStep);
                                currentSum += kMatrix[currentStep, numbMinStep];

                                currentStep = numbMinStep;
                                //if (currentStep > models.Length) currentStep = 0;
                            }
                        }

                        if (currentSum < minSum)
                        {
                            minWay = currentWay;
                            minSum = currentSum;
                        }
                        MessageBox.Show("");
                    }

                    lbSortedList.Items.Clear();
                    foreach (int i in minWay)
                    {
                        lbSortedList.Items.Add(models[i]);
                    }

                }
                //catch (Exception exc)
                //{
                //    MessageBox.Show(exc.Message);
                //}

               
            }
            ofd.Dispose();
        }


        private void несколькоФайловToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            lbOriginalList.Items.Clear();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //try
                {
                    //MessageBox.Show(Path.GetFileName(ofd.FileNames[0]) + " | " + ofd.FileNames[ofd.FileNames.Length - 1]);
                    intensMatrix = new List<List<double>>();
                    fileList = ofd.FileNames;
                    StreamReader intensFile;

                    DataTable dt = new DataTable();
                    DataRow r;
                    dt.Columns.Add("q");

                    //формируем список моделей по списку файлов
                    models = new string[ofd.FileNames.Length];
                    for (int i = 0; i < ofd.FileNames.Length; i++)
                    {
                        models[i] = Path.GetFileName(ofd.FileNames[i]);
                        models[i] = "model_" + models[i].Substring(models[i].IndexOf("model") + "model".Length + 1, models[i].Substring(models[i].IndexOf("model") + "model".Length + 1).IndexOf('_'));

                        dt.Columns.Add(models[i]);
                        lbOriginalList.Items.Add(models[i]);//Path.GetFileName(ofd.FileNames[i]));
                    }

                    string[] formatline;
                    string line;

                    //считываем интенсивности в матрицу + создаем пустую заготовку для вывода таблицы 
                    for (int i = 0; i < ofd.FileNames.Length; i++)
                    {

                        intensFile = new StreamReader(ofd.FileNames[i]);
                        intensMatrix.Add(new List<double>());
                        while ((line = intensFile.ReadLine()) != null)
                        {
                            if (line != "")
                            {
                                formatline = line.Trim().Split('\t');
                                intensMatrix[i].Add(Convert.ToDouble(formatline[1]));

                                if (i == 0)
                                {
                                    r = dt.NewRow();
                                    r["q"] = formatline[0];
                                    dt.Rows.Add(r);
                                }
                            }
                        }

                        intensFile.Close();
                    }

                    //просто выводим таблицу)
                    for (int i = 0; i < intensMatrix.Count; i++)
                    {
                        for (int j = 0; j < intensMatrix[i].Count; j++)
                        {
                            dt.Rows[j][models[i]] = intensMatrix[i][j].ToString();
                        }
                    }
                    dataGridView1.DataSource = dt;
                    dt.Dispose();
                }
            }
            
        }



        private void среднийКвадратРазностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kMatrix = new double[models.Length, models.Length];
            {
                //считаем коэффициент отличия "каждый с каждым"

                for (int i = 0; i < models.Length - 1; i++)
                {
                    kMatrix[i, i] = 0;
                    for (int j = i + 1; j < models.Length; j++)
                    {
                        kMatrix[i, j] = DerNorma(intensMatrix[i], intensMatrix[j]);
                        kMatrix[j, i] = kMatrix[i, j];
                    }
                }
            }
            kMatrixPrint();
        }

        private void среднийМодульРазностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kMatrix = new double[models.Length, models.Length];
            {
                //считаем коэффициент отличия "каждый с каждым"

                for (int i = 0; i < models.Length - 1; i++)
                {
                    kMatrix[i, i] = 0;
                    for (int j = i + 1; j < models.Length; j++)
                    {
                        for (int iq = 0; iq < intensMatrix[i].Count; iq++)
                        {
                            kMatrix[i, j] += Math.Abs(intensMatrix[i][iq] - intensMatrix[j][iq]);
                        }
                        kMatrix[i, j] /= intensMatrix[i].Count;
                        kMatrix[j, i] = kMatrix[i, j];
                    }
                }
            }
            kMatrixPrint();
        }

        private void kMatrixPrint()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("model");
            for (int i = 0; i < models.Length; dt.Columns.Add((i++).ToString())) ; //добавляем столбцы
            //выводим матрицу коэффициентов отличия 
            for (int j = 0; j < models.Length; j++)
            {
                DataRow r = dt.NewRow();
                r["model"] = j.ToString();
                for (int i = 0; i < models.Length; i++)
                {
                    r[i.ToString()] = kMatrix[i, j];
                }
                dt.Rows.Add(r);
            }
            dataGridView2.DataSource = dt;
            dt.Dispose();
        }

        private void определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Начинаем искать оптимальный переход по заданной матрице (графу) коэффициентов отличия
            List<int> minWay = new List<int>(),
                     currentWay = new List<int>();
            int currentStep;
            double minSum = double.MaxValue,
                   currentSum = 0;
            double minStep;
            int numbMinStep;
            for (int dot = 0; dot < models.Length; dot++)
            {
                currentWay.Clear();
                currentWay.Add(dot);
                currentSum = 0;

                currentStep = dot;
                for (int i = 0; i < models.Length /*- 1*/; i++)
                {
                    minStep = double.MaxValue;
                    numbMinStep = -1;

                    for (int j = 0; j < models.Length; j++)
                    {
                        if ((currentStep != j) && (kMatrix[currentStep, j] < minStep))
                        {

                            bool flag = false;
                            for (int g = 0; ((g < currentWay.Count)); g++)
                            {
                                if (!(flag = (j != currentWay[g])))
                                {
                                    break;
                                }
                                //MessageBox.Show(j.ToString() + "| [" + g.ToString() + "]" + currentWay[g].ToString());
                            }

                            if (flag)
                            {
                                minStep = kMatrix[currentStep, j];
                                numbMinStep = j;
                            }
                        }
                    }
                    //this.Refresh();
                    currentWay.Add(numbMinStep);
                    currentSum += kMatrix[currentStep, numbMinStep];

                    currentStep = numbMinStep;
                    //if (currentStep > models.Length) currentStep = 0;
                    
                }

                if (currentSum < minSum)
                {
                    minWay = currentWay;
                    minSum = currentSum;
                }
                
            }

            lbSortedList.Items.Clear();
            foreach (int i in minWay)
            {
                lbSortedList.Items.Add(models[i]);
            }
        }

        private void поПервойToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i, j;
            double intensfirst = intensMatrix[0][0];
            double intensfirsti;
            for (i = 1; i <= intensMatrix.Count - 1; i++)
            {
                intensfirsti = intensMatrix[i][0];
                for (j = 0; j <= intensMatrix[i].Count - 1; j++)
                {
                    intensMatrix[i][j] = intensMatrix[i][j] - (intensfirsti - intensfirst);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;

namespace StepByStep
{
    public partial class Form1 : Form
    {
        List<List<double>> intensMatrix, intensMatrixOriginal;
        List<double> q,qOrig;
        string[] fileList;
        double[,] kMatrix;
        List<int> models, sortedModels;
        Regex rxInt = new Regex(@"^\d+$"); // любые цифры 
        Regex rxDouble = new Regex(@"^\d+.+$"); // любые цифры и точка
        int beginQ, endQ;

        //Небольшая костылина для временного подсчёта различий
        List<List<int>> ListOfWay;

        public Form1()
        {
            InitializeComponent();

            intensMatrix = null;
            intensMatrixOriginal = null;
            q = null;
            beginQ = endQ = 0;
            models = null;
            fileList = null;
            kMatrix = null;
        }

        #region Средний квадрат разности
        private double Norma(List<double> x)
        {
            double sum = 0;
            int i;
            for (i = 0; i <= x.Count - 1; i++)
            {
                sum += x[i] * x[i];
            }
            return Math.Sqrt(sum);
        }
        private double Norma(List<double> x1, List<double> x2)
        {
            double sum = 0;
            int i;
            for (i = 0; i <= x1.Count - 1; i++)
            {
                sum += (x1[i] - x2[i]) * (x1[i] - x2[i]);
            }
            return Math.Sqrt(sum);
        }
        private double DerNorma(List<double> x1, List<double> x2)
        {
            return Norma(x1, x2) / Math.Sqrt(Norma(x1) * Norma(x2));
        }
        #endregion

        private void загрузитьФайлыToolStripMenuItem_Click(object sender, EventArgs e)//Полный цикл
        {
            несколькоФайловToolStripMenuItem_Click(sender, e);
            среднийКвадратРазностиToolStripMenuItem_Click(sender, e);
            //определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem_Click(sender, e);
            определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem_Click(sender, e);
        }


        private void несколькоФайловToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //try
                {
                    //MessageBox.Show(Path.GetFileName(ofd.FileNames[0]) + " | " + ofd.FileNames[ofd.FileNames.Length - 1]);
                    intensMatrixOriginal = new List<List<double>>();
                    qOrig = new List<double>();
                    fileList = ofd.FileNames;
                    StreamReader intensFile;


                    //формируем список моделей по списку файлов
                    models = new List<int>();
                    for (int i = 0; i < ofd.FileNames.Length; i++)
                    {
                        string modelsString = Path.GetFileName(ofd.FileNames[i]);
                        models.Add(Convert.ToInt32(modelsString.Substring(modelsString.IndexOf("model") + "model".Length + 1, modelsString.Substring(modelsString.IndexOf("model") + "model".Length + 1).IndexOf('_'))));
                    }

                    string[] formatline;
                    string line;

                    //считываем интенсивности в матрицу 
                    for (int i = 0; i < ofd.FileNames.Length; i++)
                    {

                        intensFile = new StreamReader(ofd.FileNames[i]);
                        intensMatrixOriginal.Add(new List<double>());
                        while ((line = intensFile.ReadLine()) != null)
                        {
                            if (line != "")
                            {
                                formatline = line.Trim().Split('\t');

                                if (i == 0)
                                {
                                    qOrig.Add(Convert.ToDouble(formatline[0].Trim()));
                                }

                                intensMatrixOriginal[i].Add(Math.Pow(10, Convert.ToDouble(formatline[1].Trim())));
                            }
                        }

                        intensFile.Close();
                    }

                    //intensMatrix = new List<List<double>>();
                    //for (int i = 0; i < intensMatrixOriginal.Count; i++ )
                    //{
                    //    intensMatrix.Add(new List<double>());
                    //    for (int j=0; j< intensMatrixOriginal[i].Count; j++)
                    //    {
                    //        intensMatrix[i].Add(intensMatrixOriginal[i][j]);
                    //    }
                    //}

                    //q = new List<double>();
                    //foreach (double qValue in qOrig)
                    //{
                    //    q.Add(qValue);
                    //}

                    //beginQ = 0;
                    //endQ = qOrig.Count - 1;


                    //IntensMatrixPrint();
                    вернутьИсходнуюМатрицуToolStripMenuItem_Click(sender, e);
                    printOriginalList(models);
                }
            }

        }



        private void среднийКвадратРазностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kMatrix = new double[models.Count, models.Count];
            {
                //считаем коэффициент отличия "каждый с каждым"

                for (int i = 0; i < models.Count - 1; i++)
                {
                    kMatrix[i, i] = 0;
                    for (int j = i + 1; j < models.Count; j++)
                    {
                        kMatrix[i, j] = DerNorma(intensMatrix[i], intensMatrix[j]);
                        kMatrix[j, i] = kMatrix[i, j];
                    }
                }
            }
            kMatrixPrint();
        }

        private void нормаФункциибезУсредненияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kMatrix = new double[models.Count, models.Count];
            {
                //считаем коэффициент отличия "каждый с каждым"

                for (int i = 0; i < models.Count - 1; i++)
                {
                    kMatrix[i, i] = 0;
                    for (int j = i + 1; j < models.Count; j++)
                    {
                        kMatrix[i, j] = Norma(intensMatrix[i], intensMatrix[j]);
                        kMatrix[j, i] = kMatrix[i, j];
                    }
                }
            }
            kMatrixPrint();
        }

        private void среднийМодульРазностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            kMatrix = new double[models.Count, models.Count];
            {
                //считаем коэффициент отличия "каждый с каждым"

                for (int i = 0; i < models.Count - 1; i++)
                {
                    kMatrix[i, i] = 0;
                    for (int j = i + 1; j < models.Count; j++)
                    {
                        for (int iq = 0; iq < intensMatrix[i].Count; iq++)
                        {
                            kMatrix[i, j] += Math.Abs(intensMatrix[i][iq] - intensMatrix[j][iq]);
                        }
                        kMatrix[i, j] /= intensMatrix[i].Count;
                        kMatrix[j, i] = kMatrix[i, j];
                    }
                }
            }
            kMatrixPrint();
        }

        


        #region Набор методов поиска оптимального перехода
        private List<int> FindMinOptimalWay()
        {
            List<int> result = BuildOptimalWay(0),
                      currentWay = new List<int>();
            ListOfWay = new List<List<int>>();
            ListOfWay.Add(result);

            for (int beginStep = 1; beginStep < models.Count; beginStep++)
            {
                currentWay = BuildOptimalWay(beginStep);
                ListOfWay.Add(currentWay);
                if (LengthWay(currentWay) < LengthWay(result)) result = currentWay;
            }

            //Преобразуем внутренню нумерацию во внешнюю (считанную из файла)
            List<int> convertResult = new List<int>();
            for (int i = 0; i < result.Count; i++)
            {
                //result[result.IndexOf(i)] = models[result[i]];
                convertResult.Add(models[result[i]]);
            }

            //WriteLine("___________________");
            WriteLine(String.Format("S_opt = {0:0.00000}",LengthWay(result)));
            WriteLine(String.Format("S_orig = {0:0.00000}", LengthWay(BuildOriginalWay())));
            return convertResult;
        }

        private List<int> BuildOriginalWay()
        {
            List<int> orig = new List<int>();

            for (int i = 0; i < models.Count; i++ )
            {
                orig.Add(i);
            }

            return orig;
        }

        private double LengthWay(List<int> currentWay, int k=0)
        {
            double length = 0;

            for (int i = 1; i < currentWay.Count; i++)
            {
                length += kMatrix[currentWay[i - 1]-k, currentWay[i]-k];
            }

            return length;
        }

        private List<int> BuildOptimalWay(int begin)
        {
            List<int> optimWay = new List<int>();
            int step;
            double stepLength;

            optimWay.Add(begin);
            while (optimWay.Count < models.Count)
            {
                step = FindMinStep(optimWay[optimWay.Count - 1], optimWay);
                if (step != -1)
                {
                    optimWay.Add(step);
                    stepLength = kMatrix[optimWay[optimWay.Count - 1], step]; //для вывода (по необходимости)
                }
            }

            return optimWay;
        }

        private int FindMinStep(int from, List<int> currentWay)
        {
            int minStep = -1;
            double minStepLength = double.MaxValue;

            for (int i = 0; i < models.Count; i++)
            {
                if ((from != i) && (kMatrix[from, i] < minStepLength) && (currentWay.IndexOf(i) == -1))
                {
                    minStep = i;
                    minStepLength = kMatrix[from, i];
                }
            }

            return minStep;
        }
        #endregion

        private void определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sortedModels = FindMinOptimalWay();
            printSortedList(sortedModels);

        }



        private void поПервойToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int i, j;
            double intensfirst = intensMatrix[0][0];
            double intensfirsti;
            for (i = 1; i <= intensMatrix.Count - 1; i++)
            {
                intensfirsti = intensMatrix[i][0];
                for (j = 0; j <= intensMatrix[i].Count - 1; j++)
                {
                    intensMatrix[i][j] = intensMatrix[i][j] - (intensfirsti - intensfirst);
                }
            }
            IntensMatrixPrint();
        }

        private void записатьУпорядоченныйPdbToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.pdb|*.pdb";
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "*.pdb|*.pdb";
            string line;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter outfile = new StreamWriter(sfd.FileName);

                    for (int i = 0; i < sortedModels.Count; i++)
                    {
                        StreamReader infile = new StreamReader(ofd.FileName);
                        //try
                        {
                            bool flag = false;
                            while ((line = infile.ReadLine()) != null)
                            {
                                if ((line.Length >= 14) && (line.Substring(0, 5) == "MODEL") && (Convert.ToInt32(line.Substring(10, 4)) == (sortedModels[i])))
                                {
                                    flag = true;
                                    outfile.WriteLine(String.Format("{0}{1,4}", "MODEL     ", (i + 1).ToString()));
                                    //String.Format("{0,10:0.0}", 123.4567);
                                    //1 -  6        Record name   "MODEL "
                                    //11 - 14        Integer       serial         Model serial number.
                                }
                                if (flag)
                                {
                                    if ((line.Length >= 6) && ((line.Substring(0, 4) == "ATOM") || (line.Substring(0, 6) == "HETATM")))
                                    {
                                        outfile.WriteLine(line);
                                    }

                                    if ((line.Length >= 6) && (line.Substring(0, 6) == "ENDMDL"))
                                    {
                                        outfile.WriteLine("ENDMDL");
                                        break;
                                    }
                                }


                            }
                        }

                        //catch (Exception exc)
                        //{
                        //    MessageBox.Show(exc.Message);
                        //}

                        infile.Close();
                    }
                    outfile.Close();
                }
            }

        }



        private void отЗаданногоНачалаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string value = "1";
            if (InputBox("Ввод данных", "Введите номер начальной конформации.", ref value) == DialogResult.OK)
            {
                // value - и есть введенное значение
                try
                {

                    int inputbegin = Convert.ToInt32(value);
                    int begin = models.IndexOf(inputbegin);
                    List<int> result = BuildOptimalWay(begin);
                    List<int> convertResult = new List<int>();
                    for (int i = 0; i < result.Count; i++)
                    {
                        //result[result.IndexOf(i)] = models[result[i]];
                        convertResult.Add(models[result[i]]);
                    }
                    sortedModels = convertResult;


                    //WriteLine("___________________");
                    WriteLine(String.Format("S_opt = {0:0.00000}", LengthWay(result)));
                    WriteLine(String.Format("S_orig = {0:0.00000}", LengthWay(BuildOriginalWay())));

                    printSortedList(sortedModels);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        #region Группа методов графического отображения данных на форме

        public void WriteLine(string line)
        {
            string[] subLine = line.Split('\n');
            foreach (string s in subLine)
            {
                textBox1.Text += s + Environment.NewLine;

            }
        }

        public static DialogResult InputBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }

        private void IntensMatrixPrint()//вывод матрицы интенсивностей в таблицу
        {


                    DataTable dt = new DataTable();
                    DataRow r;
                    dt.Columns.Add("q");

                    for (int i = 0; i < models.Count; dt.Columns.Add("model_" + models[i++].ToString())) ; //добавляем столбцы

                    //считываем интенсивности в матрицу + создаем пустую заготовку для вывода таблицы 
                    foreach(double qValue in q)
                    {
                        r = dt.NewRow();
                        r["q"] = qValue;
                        dt.Rows.Add(r);
                    }

                    //просто выводим таблицу)
                    for (int i = 0; i < intensMatrix.Count; i++)
                    {
                        for (int j = 0; j < intensMatrix[i].Count; j++)
                        {
                            dt.Rows[j]["model_" + models[i].ToString()] = intensMatrix[i][j].ToString();
                        }
                    }

                    dataGridView1.DataSource = dt;
                    dt.Dispose();

        }

        private void kMatrixPrint()//вывод матрицы коэффициентов отличия в таблицу
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("model");
            for (int i = 0; i < models.Count; dt.Columns.Add(models[i++].ToString())) ; //добавляем столбцы
            //выводим матрицу коэффициентов отличия 
            for (int j = 0; j < models.Count; j++)
            {
                DataRow r = dt.NewRow();
                r["model"] = models[j].ToString();
                for (int i = 0; i < models.Count; i++)
                {
                    r[models[i].ToString()] = kMatrix[i, j];
                }
                dt.Rows.Add(r);
            }
            dataGridView2.DataSource = dt;
            dt.Dispose();
        }

        private void printSortedList(List<int> list)
        {
            lbSortedList.Items.Clear();
            //WriteLine("Порядок при "+q[beginQ].ToString()+"<=q<="+q[endQ].ToString()+":");
            WriteLine(String.Format("Порядок при\n({0:0.000}<=q<={1:0.000}) A^-1\n", q[beginQ], q[endQ]));
            int prev = -1;
            foreach (int i in list)
            {
                //result[result.IndexOf(i)] = models[result[i]];
                lbSortedList.Items.Add("model_" + i.ToString());
                WriteLine(String.Format("model_{0:00} <- ({1:0.0000000})", i, (prev != -1) ? kMatrix[models.IndexOf(i), models.IndexOf(prev)] : 0));
                prev = i;
            }
            WriteLine("___________________");
        }
        private void printOriginalList(List<int> list)
        {
            lbOriginalList.Items.Clear();
            foreach (int i in list)
            {
                lbOriginalList.Items.Add("model_" + i.ToString());
            }
        }
        #endregion

        private void rMSDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Regex rxInt = new Regex(@"^\d+$"); // любые цифры 
            //Regex rxDouble = new Regex(@"^\d+.+$"); // любые цифры и точка

            //if (rxNums.IsMatch("481516.2342"))
            //{

            //    MessageBox.Show("Это число.");

            //}

            //else
            //{

            //    MessageBox.Show("Это не число.");

            //}

            string line;
            //загрузка таблицы коэффициентов из файла 
            OpenFileDialog ofgk = new OpenFileDialog();

            if (ofgk.ShowDialog() == DialogResult.OK)
            {
                if ((kMatrix == null) || (kMatrix.Length <= 0))
                {
                    MessageBox.Show("Сначала загрузите список моделей.");
                }
                else
                {
                    StreamReader matrixFile = new StreamReader(ofgk.FileName);
                    String[] sepLine;
                    try
                    {
                        while ((line = matrixFile.ReadLine()) != null)
                        {
                            if (line != "")
                            {
                                sepLine = line.Trim().Split('\t');
                                //string outline = "|";
                                //foreach (string s in sepLine)
                                //{
                                //    outline += s + "|";
                                //}
                                //MessageBox.Show(outline);
                                if (rxInt.IsMatch(sepLine[1].Trim()) && (rxInt.IsMatch(sepLine[3].Trim()) && (rxDouble.IsMatch(sepLine[4].Trim()))))
                                {
                                    int iL = Convert.ToInt32(sepLine[1].Trim());
                                    int jL = Convert.ToInt32(sepLine[3]);
                                    double valueL = Convert.ToDouble(sepLine[4]);

                                    kMatrix[iL, jL] = valueL;
                                    //MessageBox.Show(String.Format("({0};{1})={2}",iL,jL,valueL));
                                }
                            }
                        }
                        kMatrixPrint();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }
            }
        }

        private void определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem_Click(object sender, EventArgs e)
        {

            double max = double.MinValue;
            int imax = 0, jmax = 0;
            for (int i = 0; i < models.Count; i++)
            {
                for (int j = 0; j < models.Count; j++)
                {
                    if (i != j)
                    {
                        if (kMatrix[i, j] > max)
                        {
                            max = kMatrix[i, j];
                            imax = i;
                            jmax = j;
                        }
                    }
                }
            }

            int begin = imax; ;
            textBox1.Text += String.Format("{0}->{1}={2:0.000}", models[imax], models[jmax], max) + Environment.NewLine;
            //Environment.NewLine
            List<int> result = BuildOptimalWay(begin);
            List<int> convertResult = new List<int>();
            for (int i = 0; i < result.Count; i++)
            {
                //result[result.IndexOf(i)] = models[result[i]];
                convertResult.Add(models[result[i]]);
            }
            sortedModels = convertResult;

            //WriteLine("___________________");
            WriteLine(String.Format("S_opt = {0:0.00000}", LengthWay(result)));
            WriteLine(String.Format("S_orig = {0:0.00000}", LengthWay(BuildOriginalWay())));

            printSortedList(sortedModels);

        }

        private void задатьГраницыАнализаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string value = q[beginQ].ToString();
                double beginValue, endValue;
                if (InputBox("Ввод данных", "q начальное (А^-1)", ref value) == DialogResult.OK)
                {
                    if ((rxDouble.IsMatch(value))||(rxInt.IsMatch(value)))
                    {
                        beginValue = Convert.ToDouble(value);
                        if (beginValue <= q[q.Count - 1])
                        {
                            value = q[endQ].ToString();
                            if (InputBox("Ввод данных", "q конечное (А^-1)", ref value) == DialogResult.OK)
                            {
                                if ((rxDouble.IsMatch(value)) || (rxInt.IsMatch(value)))
                                {
                                    endValue = Convert.ToDouble(value);
                                    if (endValue >= beginValue)
                                    {
                                        int ibeg, iend;
                                        for (ibeg = 0; ((ibeg < q.Count) && (q[ibeg] < beginValue)); ibeg++) { }
                                        for (iend = q.Count - 1; ((iend > ibeg) && (q[iend] > endValue)); iend--) { }

                                        beginQ = ibeg;
                                        endQ = iend;

                                        MessageBox.Show(beginQ.ToString() + "|" + endQ.ToString());

                                        q.Clear();
                                        for (int j = beginQ; j <= endQ; j++)
                                        {
                                            q.Add(qOrig[j]);
                                        }

                                        for (int i = 0; i < models.Count; i++)
                                        {
                                            int size = intensMatrix.Count;
                                            intensMatrix[i].Clear();
                                            
                                            //MessageBox.Show(intensMatrixOriginal.Count.ToString() + "|" + intensMatrixOriginal[i].Count.ToString());
                                            for (int j = beginQ; j <= endQ; j++)
                                            {
                                                intensMatrix[i].Add(intensMatrixOriginal[i][j]);
                                            }
                                        }

                                        beginQ = 0;
                                        endQ = q.Count-1;

                                        IntensMatrixPrint();
                                        среднийКвадратРазностиToolStripMenuItem_Click(sender, e);
                                        //определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem_Click(sender, e);
                                        определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem_Click(sender, e);


                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Необходимо ввести дробное число (в качестве разделителя использовать точку)!");
                                }
                            }

                        }

                    }
                    else
                    {
                        MessageBox.Show("Необходимо ввести дробное число (в качестве разделителя использовать точку)!");
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void вернутьИсходнуюМатрицуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            intensMatrix = new List<List<double>>();
            for (int i = 0; i < intensMatrixOriginal.Count; i++)
            {
                intensMatrix.Add(new List<double>());
                for (int j = 0; j < intensMatrixOriginal[i].Count; j++)
                {
                    intensMatrix[i].Add(intensMatrixOriginal[i][j]);
                }
            }

            q = new List<double>();
            foreach (double qValue in qOrig)
            {
                q.Add(qValue);
            }

            beginQ = 0;
            endQ = qOrig.Count - 1;
            IntensMatrixPrint();
        }

        private void однимФайломToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //try
            {
                SaveFileDialog sfd = new SaveFileDialog();

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter file = new StreamWriter(sfd.FileName);
                    file.Write("q");
                    foreach (int j in models)
                    {
                        file.Write("\tmodel_" + j.ToString() + "_");
                    }
                    file.WriteLine();
                    for (int i = 0; i < q.Count; i++)
                    {
                        file.Write(q[i]);
                        for (int j = 0; j < models.Count; j++)
                        {
                            file.Write('\t' + Math.Log10(intensMatrix[j][i]).ToString());
                        }
                        file.WriteLine();
                    }
                    file.Close();

                }
                sfd.Dispose();
            }
            //catch(Exception exc)
            {
                // MessageBox.Show(exc.Message);
            }
        }

        private void объединитьФайлыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "*.pdb|*.pdb";
            ofd.Multiselect = true;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "*.pdb|*.pdb";
            string line;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter outfile = new StreamWriter(sfd.FileName);

                    //for (int i = 0; i < sortedModels.Count; i++)
                    int i = 0;
                    MessageBox.Show(ofd.FileNames.Length.ToString());
                    foreach (string infileName in ofd.FileNames)
                    {

                        StreamReader infile = new StreamReader(infileName);
                        //try
                        outfile.WriteLine(String.Format("{0}{1,4}", "MODEL     ", (i + 1).ToString()));

                        while ((line = infile.ReadLine()) != null)
                        {
                            if ((line.Length >= 6) && ((line.Substring(0, 4) == "ATOM") || (line.Substring(0, 6) == "HETATM")))
                            {
                                outfile.WriteLine(line);

                            }

                        }

                        outfile.WriteLine("ENDMDL");



                        //catch (Exception exc)
                        //{
                        //    MessageBox.Show(exc.Message);
                        //}

                        infile.Close();
                        i++;
                    }
                    outfile.Close();
                }
            }

        }

        private void вырезатьАктивныеАминокислотыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int beg = 326, end = 352; //диапазон активных аминокислот
            string line, firstline;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Выбор файлов конформаций белка";
            ofd.Filter = "*.pdb|*.pdb";
            ofd.Multiselect = true;
            SaveFileDialog sfdCut = new SaveFileDialog();
            sfdCut.Title = "Файл с активными аминокислотами";
            sfdCut.Filter = "*.pdb|*.pdb";
            SaveFileDialog sfdFreeze = new SaveFileDialog();
            sfdFreeze.Title = "Файл с замороженной базой";
            sfdFreeze.Filter = "*.pdb|*.pdb";


            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (sfdCut.ShowDialog() == DialogResult.OK)
                {
                    if (sfdFreeze.ShowDialog() == DialogResult.OK)
                    {
                        StreamWriter cutfile = new StreamWriter(sfdCut.FileName);
                        StreamWriter freezefile = new StreamWriter(sfdFreeze.FileName);


                        int i = 0;
                        //MessageBox.Show(ofd.FileNames.Length.ToString());
                        foreach (string infileName in ofd.FileNames)
                        {

                            StreamReader infile = new StreamReader(infileName);
                            StreamReader firstfile = new StreamReader(ofd.FileNames[0]);


                            cutfile.WriteLine(String.Format("{0}{1,4}", "MODEL     ", (i + 1).ToString()));
                            freezefile.WriteLine(String.Format("{0}{1,4}", "MODEL     ", (i + 1).ToString()));

                            do
                            {
                                line = infile.ReadLine();
                                firstline = firstfile.ReadLine();
                                if ((line != null) && (firstline != null))
                                {
                                    if (((line.Length >= 6) && ((line.Substring(0, 4) == "ATOM") || (line.Substring(0, 6) == "HETATM")))
                                        && ((firstline.Length >= 6) && ((firstline.Substring(0, 4) == "ATOM") || (firstline.Substring(0, 6) == "HETATM"))))
                                    {
                                        //if (line.Substring(0, 26) == firstline.Substring(0, 26))
                                        //нужна иная проверка соответствия остальных конформаций первой. причём с прокруткой первой!
                                        {
                                            string[] subLine = 
                                            {
                                                line.Substring(0,6).Trim(),  ///0 | 1 - 6     "ATOM  "
                                                line.Substring(6,5).Trim(),  ///1 | 7 - 11    serial       Atom  serial number.
                                                line.Substring(12,4).Trim(), ///2 | 13 - 16   name         Atom name.
                                                line.Substring(16,1).Trim(), ///3 | 17        altLoc       Alternate location indicator.
                                                line.Substring(17,3).Trim(), ///4 | 18 - 20   resName      Residue name.
                                                line.Substring(21,1).Trim(), ///5 | 22        Character     chainID      Chain identifier.
                                                line.Substring(22,4).Trim(), ///6 | 23 - 26   Integer       resSeq       Residue sequence number.
                                                line.Substring(26,1).Trim(), ///7 | 27        AChar         iCode        Code for insertion of residues.
                                                line.Substring(30,8).Trim(), ///8 | 31 - 38   Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
                                                line.Substring(38,8).Trim(), ///9 | 39 - 46   Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
                                                line.Substring(46,8).Trim(), ///10| 47 - 54   Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
                                                line.Substring(54,6).Trim(), ///11| 55 - 60   Real(6.2)     occupancy    Occupancy.
                                                line.Substring(60,6).Trim(), ///12| 61 - 66   Real(6.2)     tempFactor   Temperature  factor.
                                                line.Substring(76,2).Trim(), ///13| 77 - 78   LString(2)    element      Element symbol, right-justified.
                                                line.Substring(78,2).Trim()  ///14| 79 - 80   LString(2)    charge       Charge  on the atom.
                                            };

                                            

                                            int currentAminoAcids = Convert.ToInt32(subLine[6]);
                                                currentAminoAcids = Convert.ToInt32(subLine[6]);
                                            if ((currentAminoAcids>=beg)&&(currentAminoAcids<=end))
                                            {
                                                cutfile.WriteLine(line);
                                                freezefile.WriteLine(line);
                                            }
                                            else
                                            {
                                                freezefile.WriteLine(firstline);
                                            }
                                        }
                                        //else
                                        //{
                                        //    MessageBox.Show(String.Format("Строка модели {0} [{1}] отличается от строки [{2}]", i + 1, line, firstline));
                                        //}

                                    }
                                }

                            }
                            while (line != null);

                            cutfile.WriteLine("ENDMDL");
                            freezefile.WriteLine("ENDMDL");


                            infile.Close();
                            firstfile.Close();
                            i++;
                        }
                        cutfile.Close();
                        freezefile.Close();
                    }
                }
            }
        }

        private void заморозитьБазуПоПервойToolStripMenuItem_Click(object sender, EventArgs e)
        {
        //    int beg = 326, end = 352; //диапазон активных аминокислот
        //    string line, baseline;

        //    OpenFileDialog ofdBase = new OpenFileDialog();
        //    ofdBase.Title = "Выбор базового файла";
        //    ofdBase.Filter = "*.pdb|*.pdb";

        //    OpenFileDialog ofdInsert = new OpenFileDialog();
        //    ofdInsert.Title = "Выбор файлов с активными аминокислотами (по конформациям)";
        //    ofdInsert.Filter = "*.pdb|*.pdb";
        //    ofdInsert.Multiselect = true;

        //    SaveFileDialog sfdFreeze = new SaveFileDialog();
        //    sfdFreeze.Title = "Файл с замороженной базой";
        //    sfdFreeze.Filter = "*.pdb|*.pdb";


        //    if (ofdInsert.ShowDialog() == DialogResult.OK)
        //    {
        //        if (ofdBase.ShowDialog() == DialogResult.OK)
        //        {
        //            if (sfdFreeze.ShowDialog() == DialogResult.OK)
        //            {
        //                StreamWriter freezefile = new StreamWriter(sfdFreeze.FileName);


        //                int i = 0;
        //                //MessageBox.Show(ofd.FileNames.Length.ToString());
        //                foreach (string insertfileName in ofdInsert.FileNames)
        //                {

        //                    StreamReader insertfile = new StreamReader(insertfileName);
        //                    StreamReader basefile = new StreamReader(ofdBase.FileNames[0]);


        //                    freezefile.WriteLine(String.Format("{0}{1,4}", "MODEL     ", (i + 1).ToString()));

        //                    do
        //                    {
        //                        baseline = basefile.ReadLine();
        //                        if (baseline != null)
        //                        {
        //                            if ((baseline.Length >= 6) && ((baseline.Substring(0, 4) == "ATOM") || (baseline.Substring(0, 6) == "HETATM")))
        //                            {
        //                                //((line.Length >= 6) && ((line.Substring(0, 4) == "ATOM") || (line.Substring(0, 6) == "HETATM"))
                                        
        //                                {
                                            

        //                                    string[] subLineBase = 
        //                                    {
        //                                        baseline.Substring(0,6).Trim(),  ///0 | 1 - 6     "ATOM  "
        //                                        baseline.Substring(6,5).Trim(),  ///1 | 7 - 11    serial       Atom  serial number.
        //                                        baseline.Substring(12,4).Trim(), ///2 | 13 - 16   name         Atom name.
        //                                        baseline.Substring(16,1).Trim(), ///3 | 17        altLoc       Alternate location indicator.
        //                                        baseline.Substring(17,3).Trim(), ///4 | 18 - 20   resName      Residue name.
        //                                        baseline.Substring(21,1).Trim(), ///5 | 22        Character     chainID      Chain identifier.
        //                                        baseline.Substring(22,4).Trim(), ///6 | 23 - 26   Integer       resSeq       Residue sequence number.
        //                                        baseline.Substring(26,1).Trim(), ///7 | 27        AChar         iCode        Code for insertion of residues.
        //                                        baseline.Substring(30,8).Trim(), ///8 | 31 - 38   Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
        //                                        baseline.Substring(38,8).Trim(), ///9 | 39 - 46   Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
        //                                        baseline.Substring(46,8).Trim(), ///10| 47 - 54   Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
        //                                        baseline.Substring(54,6).Trim(), ///11| 55 - 60   Real(6.2)     occupancy    Occupancy.
        //                                        baseline.Substring(60,6).Trim(), ///12| 61 - 66   Real(6.2)     tempFactor   Temperature  factor.
        //                                        baseline.Substring(76,2).Trim(), ///13| 77 - 78   LString(2)    element      Element symbol, right-justified.
        //                                        baseline.Substring(78,2).Trim()  ///14| 79 - 80   LString(2)    charge       Charge  on the atom.
        //                                    };

        //                                    int currentAminoAcidsBase = Convert.ToInt32(subLineBase[6]);
        //                                    currentAminoAcidsBase = Convert.ToInt32(subLineBase[6]);
        //                                    if ((currentAminoAcidsBase < beg) || (currentAminoAcidsBase > end))
        //                                    {
                                                
        //                                    }
        //                                    else
        //                                    {
        //                                        freezefile.WriteLine(baseline);
        //                                    }

        //                                    string[] subLine = 
        //                                    {
        //                                        line.Substring(0,6).Trim(),  ///0 | 1 - 6     "ATOM  "
        //                                        line.Substring(6,5).Trim(),  ///1 | 7 - 11    serial       Atom  serial number.
        //                                        line.Substring(12,4).Trim(), ///2 | 13 - 16   name         Atom name.
        //                                        line.Substring(16,1).Trim(), ///3 | 17        altLoc       Alternate location indicator.
        //                                        line.Substring(17,3).Trim(), ///4 | 18 - 20   resName      Residue name.
        //                                        line.Substring(21,1).Trim(), ///5 | 22        Character     chainID      Chain identifier.
        //                                        line.Substring(22,4).Trim(), ///6 | 23 - 26   Integer       resSeq       Residue sequence number.
        //                                        line.Substring(26,1).Trim(), ///7 | 27        AChar         iCode        Code for insertion of residues.
        //                                        line.Substring(30,8).Trim(), ///8 | 31 - 38   Real(8.3)     x            Orthogonal coordinates for X in Angstroms.
        //                                        line.Substring(38,8).Trim(), ///9 | 39 - 46   Real(8.3)     y            Orthogonal coordinates for Y in Angstroms.
        //                                        line.Substring(46,8).Trim(), ///10| 47 - 54   Real(8.3)     z            Orthogonal coordinates for Z in Angstroms.
        //                                        line.Substring(54,6).Trim(), ///11| 55 - 60   Real(6.2)     occupancy    Occupancy.
        //                                        line.Substring(60,6).Trim(), ///12| 61 - 66   Real(6.2)     tempFactor   Temperature  factor.
        //                                        line.Substring(76,2).Trim(), ///13| 77 - 78   LString(2)    element      Element symbol, right-justified.
        //                                        line.Substring(78,2).Trim()  ///14| 79 - 80   LString(2)    charge       Charge  on the atom.
        //                                    };

        //                                    int currentAminoAcids = Convert.ToInt32(subLine[6]);
        //                                    currentAminoAcids = Convert.ToInt32(subLine[6]);
        //                                    if ((currentAminoAcids >= beg) && (currentAminoAcids <= end))
        //                                    {
        //                                        freezefile.WriteLine(line);
        //                                    }
        //                                    else
        //                                    {
        //                                        freezefile.WriteLine(baseline);
        //                                    }
        //                                }
        //                                //else
        //                                //{
        //                                //    MessageBox.Show(String.Format("Строка модели {0} [{1}] отличается от строки [{2}]", i + 1, line, firstline));
        //                                //}

        //                            }
        //                        }

        //                    }
        //                    while (line != null);

        //                    freezefile.WriteLine("ENDMDL");


        //                    insertfile.Close();
        //                    basefile.Close();
        //                    i++;
        //                }
        //                freezefile.Close();
        //            }
        //        }
        //    }
        }

        private void сохранитьToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void дляКриптоанализатораToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter file = new StreamWriter(sfd.FileName);
                    double maxInkMatrix = 0,
                           maxInGrapgoan = 5000,
                           kTrans = 1;

                    for (int i = 0; i < models.Count; i++)
                    {
                        for (int j = 0; j < models.Count; j++)
                        {
                            if (kMatrix[i, j] > maxInkMatrix) maxInkMatrix = kMatrix[i, j];
                        }
                    }

                    kTrans = maxInGrapgoan / maxInkMatrix;

                    for (int i = 0; i < models.Count; i++)
                    {
                        for (int j = 0; j < models.Count; j++)
                        {
                            file.Write(((int)(kMatrix[i, j] * kTrans)).ToString());
                            if (j < models.Count - 1) file.Write(",");
                        }
                        file.WriteLine();
                    }
                    file.Close();

                }
                sfd.Dispose();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void текстToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter file = new StreamWriter(sfd.FileName);
                    foreach (int j in models)
                    {
                        file.Write("model_" + j.ToString() + '\t');
                    }
                    file.WriteLine();
                    for (int i = 0; i < models.Count; i++)
                    {
                        for (int j = 0; j < models.Count; j++)
                        {
                            file.Write((kMatrix[i, j]).ToString() + '\t');
                        }
                        file.WriteLine();
                    }
                    file.Close();

                }
                sfd.Dispose();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void отчётToolStripMenuItem_Click(object sender, EventArgs e) //Метод записи отчёта для построения графика (Ордината: упорядоченные номера, Абсцисса: изначальные номера)
        {
            if ((models != null) && (sortedModels != null) && (models.Count == sortedModels.Count))
            {
                try
                {
                    string ProteinName = "";
                    SaveFileDialog sfd = new SaveFileDialog();

                    //Определяем имя белка
                    if (fileList != null)
                    {
                        ProteinName = Path.GetFileName(fileList[0]).Substring(7, Path.GetFileName(fileList[0]).IndexOf("_model") - 7);
                    }
                    sfd.Title = "Сохранение отчёта";
                    sfd.FileName = "ReportOnOrdered_"+ProteinName+".txt";
                    sfd.DefaultExt = "txt";

                    //Определяем, необходима ли инверсия выходных данных (то есть определяем наклон аппроксимирующей кривой, весьма коряво, кстати)
                    int growthSum=0;
                    for (int i = 1; i < sortedModels.Count; i++ )
                    {
                        growthSum += sortedModels[i] - sortedModels[i - 1];
                    }

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        StreamWriter file = new StreamWriter(sfd.FileName);

                        file.WriteLine("Protein\t{0}", ProteinName);
                        file.WriteLine("Weight_OriginalWay\t{0:0.0000}", LengthWay(models,1));
                        file.WriteLine("Weight_OrderedWay\t{0:0.0000}", LengthWay(sortedModels,1));
                        file.WriteLine("Q\t({0:0.0000};{1:0.0000})", q[beginQ], q[endQ]);
                        file.WriteLine("");
                        file.WriteLine("Original_{0}\tOrdered_{0}",ProteinName);
                        int j = 0;
                        for (int i = 0; i < models.Count; i++ )
                        {
                            if (growthSum>=0)
                            {
                                j = i;
                            }
                            else
                            {
                                j = (sortedModels.Count - 1) - i;
                            }

                            file.WriteLine("{0}\t{1}", models[i], sortedModels[j]);
                        }

                        file.Close();

                    }
                    sfd.Dispose();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void поискСреднегоОтклоненияВПроцентахToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double delta = 0;
            int n=models.Count;
            for (int i=1; i<n; i++)
            {
                for (int j=0; j<i; j++)
                {
                    delta += kMatrix[i, j];
                }
            }
            delta = (delta / ((n * n - n) / 2)) * 100;
            MessageBox.Show(delta.ToString());
        }

        private void выводДлинВсехНайденныхПутейToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string report = "";
            foreach (List<int> currentWay in ListOfWay)
            {
                report += //"(" + (currentWay[0]+1).ToString() + ")->(" + (currentWay[currentWay.Count-1]+1).ToString()+")=" + 
                    LengthWay(currentWay).ToString() + '\t';
            }
            //report += (LengthWay(ListOfWay[ListOfWay.Count - 1]) < LengthWay(ListOfWay[0])).ToString() + '\t';
            //report += (LengthWay(ListOfWay[ListOfWay.Count - 1])-LengthWay(ListOfWay[0])).ToString() + '\t';

            //MessageBox.Show(report);
            textBox1.Text += Environment.NewLine + report;
        }

        

        //private void удалениеМаксимальногоРебраToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    while (FindMinOptimalWay() != null)
        //    {
        //        int imax = 0, jmax = 0;

        //        for (int i = 0; i < models.Count; i++)
        //        {
        //            for (int j = 0; j < models.Count; j++)
        //            {
        //                if (kMatrix[i, j] > kMatrix[imax, jmax])
        //                {
        //                    imax = i;
        //                    jmax = j;
        //                }
        //            }
        //        }
        //        kMatrix[imax, jmax] = 0;
        //        kMatrix[jmax, imax] = 0;
        //    }
        //    kMatrixPrint();
        //}

    }
}


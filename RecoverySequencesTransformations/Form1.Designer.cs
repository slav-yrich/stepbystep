﻿namespace StepByStep
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.загрузитьФайлыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbOriginalList = new System.Windows.Forms.ListBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.lbSortedList = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.загрузитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.несколькоФайловToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.одинФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.однимФайломToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.действияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нормироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поПервойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поЗаданнойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поНулюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.задатьГраницыАнализаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вернутьИсходнуюМатрицуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.menuStrip3 = new System.Windows.Forms.MenuStrip();
            this.загрузитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.матрицуКоэффициентовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rMSDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.дляКриптоанализатораToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.текстToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.рассчитатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.среднийМодульРазностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.среднийКвадратРазностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.нормаФункциибезУсредненияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.алгоритмыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip4 = new System.Windows.Forms.MenuStrip();
            this.сохранитьToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.преобразоватьPdbToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.объединитьФайлыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.w8ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вырезатьАктивныеАминокислотыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.заморозитьБазуПоПервойToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчётToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.построитьПорядокПереходаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отЗаданногоНачалаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.алгоритмыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.выводДлинВсехНайденныхПутейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.menuStrip3.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.menuStrip4.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.загрузитьФайлыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(675, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // загрузитьФайлыToolStripMenuItem
            // 
            this.загрузитьФайлыToolStripMenuItem.Name = "загрузитьФайлыToolStripMenuItem";
            this.загрузитьФайлыToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.загрузитьФайлыToolStripMenuItem.Text = "Текущий стандарт";
            this.загрузитьФайлыToolStripMenuItem.Click += new System.EventHandler(this.загрузитьФайлыToolStripMenuItem_Click);
            // 
            // lbOriginalList
            // 
            this.lbOriginalList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOriginalList.FormattingEnabled = true;
            this.lbOriginalList.Location = new System.Drawing.Point(0, 0);
            this.lbOriginalList.Name = "lbOriginalList";
            this.lbOriginalList.Size = new System.Drawing.Size(191, 509);
            this.lbOriginalList.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(661, 509);
            this.dataGridView1.TabIndex = 2;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(3, 27);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(661, 509);
            this.dataGridView2.TabIndex = 3;
            // 
            // lbSortedList
            // 
            this.lbSortedList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSortedList.FormattingEnabled = true;
            this.lbSortedList.Location = new System.Drawing.Point(0, 0);
            this.lbSortedList.Name = "lbSortedList";
            this.lbSortedList.Size = new System.Drawing.Size(207, 509);
            this.lbSortedList.TabIndex = 4;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(675, 565);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Controls.Add(this.menuStrip2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(667, 539);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Матрица интенсивностей";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.загрузитьToolStripMenuItem,
            this.сохранитьToolStripMenuItem,
            this.действияToolStripMenuItem});
            this.menuStrip2.Location = new System.Drawing.Point(3, 3);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(661, 24);
            this.menuStrip2.TabIndex = 0;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // загрузитьToolStripMenuItem
            // 
            this.загрузитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.несколькоФайловToolStripMenuItem,
            this.одинФайлToolStripMenuItem});
            this.загрузитьToolStripMenuItem.Name = "загрузитьToolStripMenuItem";
            this.загрузитьToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.загрузитьToolStripMenuItem.Text = "Загрузить ";
            // 
            // несколькоФайловToolStripMenuItem
            // 
            this.несколькоФайловToolStripMenuItem.Name = "несколькоФайловToolStripMenuItem";
            this.несколькоФайловToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.несколькоФайловToolStripMenuItem.Text = "Несколько файлов";
            this.несколькоФайловToolStripMenuItem.Click += new System.EventHandler(this.несколькоФайловToolStripMenuItem_Click);
            // 
            // одинФайлToolStripMenuItem
            // 
            this.одинФайлToolStripMenuItem.Name = "одинФайлToolStripMenuItem";
            this.одинФайлToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.одинФайлToolStripMenuItem.Text = "(нет)Матрица ";
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.однимФайломToolStripMenuItem});
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.сохранитьToolStripMenuItem.Text = "Сохранить";
            // 
            // однимФайломToolStripMenuItem
            // 
            this.однимФайломToolStripMenuItem.Name = "однимФайломToolStripMenuItem";
            this.однимФайломToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.однимФайломToolStripMenuItem.Text = "Одним файлом";
            this.однимФайломToolStripMenuItem.Click += new System.EventHandler(this.однимФайломToolStripMenuItem_Click);
            // 
            // действияToolStripMenuItem
            // 
            this.действияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нормироватьToolStripMenuItem,
            this.задатьГраницыАнализаToolStripMenuItem,
            this.вернутьИсходнуюМатрицуToolStripMenuItem});
            this.действияToolStripMenuItem.Name = "действияToolStripMenuItem";
            this.действияToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.действияToolStripMenuItem.Text = "Изменить";
            // 
            // нормироватьToolStripMenuItem
            // 
            this.нормироватьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поПервойToolStripMenuItem,
            this.поЗаданнойToolStripMenuItem,
            this.поНулюToolStripMenuItem});
            this.нормироватьToolStripMenuItem.Name = "нормироватьToolStripMenuItem";
            this.нормироватьToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.нормироватьToolStripMenuItem.Text = "Нормировать";
            // 
            // поПервойToolStripMenuItem
            // 
            this.поПервойToolStripMenuItem.Name = "поПервойToolStripMenuItem";
            this.поПервойToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.поПервойToolStripMenuItem.Text = "По первой";
            this.поПервойToolStripMenuItem.Click += new System.EventHandler(this.поПервойToolStripMenuItem_Click);
            // 
            // поЗаданнойToolStripMenuItem
            // 
            this.поЗаданнойToolStripMenuItem.Name = "поЗаданнойToolStripMenuItem";
            this.поЗаданнойToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.поЗаданнойToolStripMenuItem.Text = "(нет)По заданной";
            // 
            // поНулюToolStripMenuItem
            // 
            this.поНулюToolStripMenuItem.Name = "поНулюToolStripMenuItem";
            this.поНулюToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.поНулюToolStripMenuItem.Text = "(нет)По нулю";
            // 
            // задатьГраницыАнализаToolStripMenuItem
            // 
            this.задатьГраницыАнализаToolStripMenuItem.Name = "задатьГраницыАнализаToolStripMenuItem";
            this.задатьГраницыАнализаToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.задатьГраницыАнализаToolStripMenuItem.Text = "Задать границы анализа";
            this.задатьГраницыАнализаToolStripMenuItem.Click += new System.EventHandler(this.задатьГраницыАнализаToolStripMenuItem_Click);
            // 
            // вернутьИсходнуюМатрицуToolStripMenuItem
            // 
            this.вернутьИсходнуюМатрицуToolStripMenuItem.Name = "вернутьИсходнуюМатрицуToolStripMenuItem";
            this.вернутьИсходнуюМатрицуToolStripMenuItem.Size = new System.Drawing.Size(225, 22);
            this.вернутьИсходнуюМатрицуToolStripMenuItem.Text = "Вернуть исходную матрицу";
            this.вернутьИсходнуюМатрицуToolStripMenuItem.Click += new System.EventHandler(this.вернутьИсходнуюМатрицуToolStripMenuItem_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Controls.Add(this.menuStrip3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(667, 539);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Матрица коэффициентов отличия";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // menuStrip3
            // 
            this.menuStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.загрузитьToolStripMenuItem1,
            this.сохранитьToolStripMenuItem1,
            this.рассчитатьToolStripMenuItem,
            this.алгоритмыToolStripMenuItem});
            this.menuStrip3.Location = new System.Drawing.Point(3, 3);
            this.menuStrip3.Name = "menuStrip3";
            this.menuStrip3.Size = new System.Drawing.Size(661, 24);
            this.menuStrip3.TabIndex = 0;
            this.menuStrip3.Text = "menuStrip3";
            // 
            // загрузитьToolStripMenuItem1
            // 
            this.загрузитьToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.матрицуКоэффициентовToolStripMenuItem,
            this.rMSDToolStripMenuItem});
            this.загрузитьToolStripMenuItem1.Name = "загрузитьToolStripMenuItem1";
            this.загрузитьToolStripMenuItem1.Size = new System.Drawing.Size(73, 20);
            this.загрузитьToolStripMenuItem1.Text = "Загрузить";
            // 
            // матрицуКоэффициентовToolStripMenuItem
            // 
            this.матрицуКоэффициентовToolStripMenuItem.Name = "матрицуКоэффициентовToolStripMenuItem";
            this.матрицуКоэффициентовToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.матрицуКоэффициентовToolStripMenuItem.Text = "(нет)Матрицу коэффициентов";
            // 
            // rMSDToolStripMenuItem
            // 
            this.rMSDToolStripMenuItem.Name = "rMSDToolStripMenuItem";
            this.rMSDToolStripMenuItem.Size = new System.Drawing.Size(241, 22);
            this.rMSDToolStripMenuItem.Text = "RMSD";
            this.rMSDToolStripMenuItem.Click += new System.EventHandler(this.rMSDToolStripMenuItem_Click);
            // 
            // сохранитьToolStripMenuItem1
            // 
            this.сохранитьToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.дляКриптоанализатораToolStripMenuItem,
            this.текстToolStripMenuItem});
            this.сохранитьToolStripMenuItem1.Name = "сохранитьToolStripMenuItem1";
            this.сохранитьToolStripMenuItem1.Size = new System.Drawing.Size(77, 20);
            this.сохранитьToolStripMenuItem1.Text = "Сохранить";
            this.сохранитьToolStripMenuItem1.Click += new System.EventHandler(this.сохранитьToolStripMenuItem1_Click);
            // 
            // дляКриптоанализатораToolStripMenuItem
            // 
            this.дляКриптоанализатораToolStripMenuItem.Name = "дляКриптоанализатораToolStripMenuItem";
            this.дляКриптоанализатораToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.дляКриптоанализатораToolStripMenuItem.Text = "Для графоанализатора";
            this.дляКриптоанализатораToolStripMenuItem.Click += new System.EventHandler(this.дляКриптоанализатораToolStripMenuItem_Click);
            // 
            // текстToolStripMenuItem
            // 
            this.текстToolStripMenuItem.Name = "текстToolStripMenuItem";
            this.текстToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.текстToolStripMenuItem.Text = "Записать матрицу";
            this.текстToolStripMenuItem.Click += new System.EventHandler(this.текстToolStripMenuItem_Click);
            // 
            // рассчитатьToolStripMenuItem
            // 
            this.рассчитатьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.среднийМодульРазностиToolStripMenuItem,
            this.среднийКвадратРазностиToolStripMenuItem,
            this.нормаФункциибезУсредненияToolStripMenuItem});
            this.рассчитатьToolStripMenuItem.Name = "рассчитатьToolStripMenuItem";
            this.рассчитатьToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.рассчитатьToolStripMenuItem.Text = "Рассчитать";
            // 
            // среднийМодульРазностиToolStripMenuItem
            // 
            this.среднийМодульРазностиToolStripMenuItem.Name = "среднийМодульРазностиToolStripMenuItem";
            this.среднийМодульРазностиToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.среднийМодульРазностиToolStripMenuItem.Text = "Средний модуль разности";
            this.среднийМодульРазностиToolStripMenuItem.Click += new System.EventHandler(this.среднийМодульРазностиToolStripMenuItem_Click);
            // 
            // среднийКвадратРазностиToolStripMenuItem
            // 
            this.среднийКвадратРазностиToolStripMenuItem.Name = "среднийКвадратРазностиToolStripMenuItem";
            this.среднийКвадратРазностиToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.среднийКвадратРазностиToolStripMenuItem.Text = "Норма функции";
            this.среднийКвадратРазностиToolStripMenuItem.Click += new System.EventHandler(this.среднийКвадратРазностиToolStripMenuItem_Click);
            // 
            // нормаФункциибезУсредненияToolStripMenuItem
            // 
            this.нормаФункциибезУсредненияToolStripMenuItem.Name = "нормаФункциибезУсредненияToolStripMenuItem";
            this.нормаФункциибезУсредненияToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.нормаФункциибезУсредненияToolStripMenuItem.Text = "Норма функции(без усреднения)";
            this.нормаФункциибезУсредненияToolStripMenuItem.Click += new System.EventHandler(this.нормаФункциибезУсредненияToolStripMenuItem_Click);
            // 
            // алгоритмыToolStripMenuItem
            // 
            this.алгоритмыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem});
            this.алгоритмыToolStripMenuItem.Name = "алгоритмыToolStripMenuItem";
            this.алгоритмыToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.алгоритмыToolStripMenuItem.Text = "Алгоритмы";
            // 
            // поискСреднегоОтклоненияВПроцентахToolStripMenuItem
            // 
            this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem.Name = "поискСреднегоОтклоненияВПроцентахToolStripMenuItem";
            this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem.Size = new System.Drawing.Size(299, 22);
            this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem.Text = "Поиск среднего отклонения в процентах";
            this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem.Click += new System.EventHandler(this.поискСреднегоОтклоненияВПроцентахToolStripMenuItem_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.splitContainer1);
            this.tabPage3.Controls.Add(this.menuStrip4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(667, 539);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Построение порядка перехода";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 27);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.textBox1);
            this.splitContainer1.Size = new System.Drawing.Size(661, 509);
            this.splitContainer1.SplitterDistance = 402;
            this.splitContainer1.TabIndex = 5;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.lbOriginalList);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.lbSortedList);
            this.splitContainer2.Size = new System.Drawing.Size(402, 509);
            this.splitContainer2.SplitterDistance = 191;
            this.splitContainer2.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(255, 509);
            this.textBox1.TabIndex = 0;
            // 
            // menuStrip4
            // 
            this.menuStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сохранитьToolStripMenuItem2,
            this.построитьПорядокПереходаToolStripMenuItem,
            this.алгоритмыToolStripMenuItem1});
            this.menuStrip4.Location = new System.Drawing.Point(3, 3);
            this.menuStrip4.Name = "menuStrip4";
            this.menuStrip4.Size = new System.Drawing.Size(661, 24);
            this.menuStrip4.TabIndex = 0;
            this.menuStrip4.Text = "menuStrip4";
            // 
            // сохранитьToolStripMenuItem2
            // 
            this.сохранитьToolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.преобразоватьPdbToolStripMenuItem,
            this.объединитьФайлыToolStripMenuItem,
            this.w8ToolStripMenuItem,
            this.отчётToolStripMenuItem});
            this.сохранитьToolStripMenuItem2.Name = "сохранитьToolStripMenuItem2";
            this.сохранитьToolStripMenuItem2.Size = new System.Drawing.Size(77, 20);
            this.сохранитьToolStripMenuItem2.Text = "Сохранить";
            // 
            // преобразоватьPdbToolStripMenuItem
            // 
            this.преобразоватьPdbToolStripMenuItem.Name = "преобразоватьPdbToolStripMenuItem";
            this.преобразоватьPdbToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.преобразоватьPdbToolStripMenuItem.Text = "Записать упорядоченный pdb ";
            this.преобразоватьPdbToolStripMenuItem.Click += new System.EventHandler(this.записатьУпорядоченныйPdbToolStripMenuItem_Click);
            // 
            // объединитьФайлыToolStripMenuItem
            // 
            this.объединитьФайлыToolStripMenuItem.Name = "объединитьФайлыToolStripMenuItem";
            this.объединитьФайлыToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.объединитьФайлыToolStripMenuItem.Text = "Объединить файлы pdb";
            this.объединитьФайлыToolStripMenuItem.Click += new System.EventHandler(this.объединитьФайлыToolStripMenuItem_Click);
            // 
            // w8ToolStripMenuItem
            // 
            this.w8ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вырезатьАктивныеАминокислотыToolStripMenuItem,
            this.заморозитьБазуПоПервойToolStripMenuItem});
            this.w8ToolStripMenuItem.Name = "w8ToolStripMenuItem";
            this.w8ToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.w8ToolStripMenuItem.Text = "(временно)2W8 ";
            // 
            // вырезатьАктивныеАминокислотыToolStripMenuItem
            // 
            this.вырезатьАктивныеАминокислотыToolStripMenuItem.Name = "вырезатьАктивныеАминокислотыToolStripMenuItem";
            this.вырезатьАктивныеАминокислотыToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.вырезатьАктивныеАминокислотыToolStripMenuItem.Text = "Вырезать активные аминокислоты";
            this.вырезатьАктивныеАминокислотыToolStripMenuItem.Click += new System.EventHandler(this.вырезатьАктивныеАминокислотыToolStripMenuItem_Click);
            // 
            // заморозитьБазуПоПервойToolStripMenuItem
            // 
            this.заморозитьБазуПоПервойToolStripMenuItem.Name = "заморозитьБазуПоПервойToolStripMenuItem";
            this.заморозитьБазуПоПервойToolStripMenuItem.Size = new System.Drawing.Size(266, 22);
            this.заморозитьБазуПоПервойToolStripMenuItem.Text = "(нет)Заморозить базу по первой";
            this.заморозитьБазуПоПервойToolStripMenuItem.Click += new System.EventHandler(this.заморозитьБазуПоПервойToolStripMenuItem_Click);
            // 
            // отчётToolStripMenuItem
            // 
            this.отчётToolStripMenuItem.Name = "отчётToolStripMenuItem";
            this.отчётToolStripMenuItem.Size = new System.Drawing.Size(243, 22);
            this.отчётToolStripMenuItem.Text = "Отчёт";
            this.отчётToolStripMenuItem.Click += new System.EventHandler(this.отчётToolStripMenuItem_Click);
            // 
            // построитьПорядокПереходаToolStripMenuItem
            // 
            this.построитьПорядокПереходаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem});
            this.построитьПорядокПереходаToolStripMenuItem.Name = "построитьПорядокПереходаToolStripMenuItem";
            this.построитьПорядокПереходаToolStripMenuItem.Size = new System.Drawing.Size(180, 20);
            this.построитьПорядокПереходаToolStripMenuItem.Text = "Построить порядок перехода";
            // 
            // методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem
            // 
            this.методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отЗаданногоНачалаToolStripMenuItem,
            this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem,
            this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem});
            this.методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem.Name = "методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem";
            this.методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem.Text = "Метод ближайших пар";
            // 
            // отЗаданногоНачалаToolStripMenuItem
            // 
            this.отЗаданногоНачалаToolStripMenuItem.Name = "отЗаданногоНачалаToolStripMenuItem";
            this.отЗаданногоНачалаToolStripMenuItem.Size = new System.Drawing.Size(379, 22);
            this.отЗаданногоНачалаToolStripMenuItem.Text = "От заданного начала";
            this.отЗаданногоНачалаToolStripMenuItem.Click += new System.EventHandler(this.отЗаданногоНачалаToolStripMenuItem_Click);
            // 
            // определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem
            // 
            this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem.Name = "определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem";
            this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem.Size = new System.Drawing.Size(379, 22);
            this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem.Text = "Определить начало (по минимальному пути перехода)";
            this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem.Click += new System.EventHandler(this.определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem_Click);
            // 
            // определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem
            // 
            this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem.Name = "определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem";
            this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem.Size = new System.Drawing.Size(379, 22);
            this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem.Text = "Определить начало по максимально удалённой паре";
            this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem.Click += new System.EventHandler(this.определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem_Click);
            // 
            // алгоритмыToolStripMenuItem1
            // 
            this.алгоритмыToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выводДлинВсехНайденныхПутейToolStripMenuItem});
            this.алгоритмыToolStripMenuItem1.Name = "алгоритмыToolStripMenuItem1";
            this.алгоритмыToolStripMenuItem1.Size = new System.Drawing.Size(83, 20);
            this.алгоритмыToolStripMenuItem1.Text = "Алгоритмы";
            // 
            // выводДлинВсехНайденныхПутейToolStripMenuItem
            // 
            this.выводДлинВсехНайденныхПутейToolStripMenuItem.Name = "выводДлинВсехНайденныхПутейToolStripMenuItem";
            this.выводДлинВсехНайденныхПутейToolStripMenuItem.Size = new System.Drawing.Size(262, 22);
            this.выводДлинВсехНайденныхПутейToolStripMenuItem.Text = "Вывод длин всех найденных путей";
            this.выводДлинВсехНайденныхПутейToolStripMenuItem.Click += new System.EventHandler(this.выводДлинВсехНайденныхПутейToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 589);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.menuStrip3.ResumeLayout(false);
            this.menuStrip3.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.menuStrip4.ResumeLayout(false);
            this.menuStrip4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem загрузитьФайлыToolStripMenuItem;
        private System.Windows.Forms.ListBox lbOriginalList;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ListBox lbSortedList;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem загрузитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem несколькоФайловToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem одинФайлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem действияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нормироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поПервойToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поЗаданнойToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поНулюToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.MenuStrip menuStrip3;
        private System.Windows.Forms.ToolStripMenuItem загрузитьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem матрицуКоэффициентовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem рассчитатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem среднийМодульРазностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem среднийКвадратРазностиToolStripMenuItem;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.MenuStrip menuStrip4;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem построитьПорядокПереходаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem методБлижайшихПарПоКратчайшейСуммеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отЗаданногоНачалаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem определитьНачалопоМинимальномуПутиПереходаToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripMenuItem преобразоватьPdbToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripMenuItem rMSDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem определитьНачалоПоМаксимальноУдалённойПареToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem задатьГраницыАнализаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вернутьИсходнуюМатрицуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem однимФайломToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem объединитьФайлыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem w8ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вырезатьАктивныеАминокислотыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem заморозитьБазуПоПервойToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem дляКриптоанализатораToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem текстToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem алгоритмыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem нормаФункциибезУсредненияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчётToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поискСреднегоОтклоненияВПроцентахToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem алгоритмыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem выводДлинВсехНайденныхПутейToolStripMenuItem;

    }
}

